package com.seankang;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {

    private String path;
    private Properties prop = new Properties();

    public Settings(String configPath){
        path = configPath;
        load();
    }


    private void load() {
        try  {
            InputStream input = new FileInputStream(path);
            // load a properties file
            prop.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public String getProperty(String key) {
        return prop.getProperty(key);
    }
}
