package com.seankang;

public class Main {


    public static void main(String [] args){
        GeocodeUtil util = new GeocodeUtil();

        Double lat = new Double(37.3480535);
        Double lon = new Double(-121.81152833333333);
        String result = util.locate(lat, lon);
        System.out.println(result);
    }
}
