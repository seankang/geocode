package com.seankang;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.json.JSONObject;


import java.util.Arrays;


public class GeocodeUtil {

    private  GeoApiContext context;

    private static String API_KEY = "";

    public GeocodeUtil() {
        Settings config = new Settings("src/main/resources/config.properties");
        API_KEY = config.getProperty("API_KEY");

        this.context = new GeoApiContext.Builder().apiKey(API_KEY).build();
    }

    public String locate(final Double latitude, final Double longitude) {


        JSONObject jo = new JSONObject();


        jo.put("latitude", latitude);
        jo.put("longitude", longitude);



        final LatLng latlng = new LatLng(latitude, longitude);
        try {
            String keyBase = "result";
            final GeocodingResult[] results = GeocodingApi.reverseGeocode(this.context, latlng).await();
            if (results != null && results.length > 0) {
                for (int i = 0; i < results.length; i++){
                    String keyname = keyBase + i;
                    System.out.println(results[i].formattedAddress);
                    jo.put(keyname, results[i].formattedAddress);
                }

            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return jo.toString();
    }


}
